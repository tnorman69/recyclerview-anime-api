package com.alecbrando.homeworkrecyclerview.model

import android.util.Log
import com.alecbrando.homeworkrecyclerview.model.models.Animes
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.alecbrando.homeworkrecyclerview.util.ResourceWrapper
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import java.lang.Exception

class Repo {

    val animeAPI by lazy { API.retrofitInstance }
//
    suspend fun bundleAnimes() = withContext(Dispatchers.IO) {
        return@withContext try {
            val response = animeAPI.fetchAnimes()
            if (response.isSuccessful && response.body()!!.animeList.isNotEmpty()) {
                ResourceWrapper.Success(response.body()!!.animeList)
            }
            else {
                ResourceWrapper.Error("Something went wrong...")
            }
        }
        catch (e: Exception) {
            Log.d("e", "bundleAnimes: $e")
            ResourceWrapper.Error(e.localizedMessage?.toString())
        }
    }

}