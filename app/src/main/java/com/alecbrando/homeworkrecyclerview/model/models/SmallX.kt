package com.alecbrando.homeworkrecyclerview.model.models

data class SmallX(
    val height: Int,
    val width: Int
)