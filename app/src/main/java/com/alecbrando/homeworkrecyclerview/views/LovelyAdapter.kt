package com.alecbrando.homeworkrecyclerview.views

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.Navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.alecbrando.homeworkrecyclerview.databinding.AnimeBinding
import com.alecbrando.homeworkrecyclerview.model.models.Data
import com.bumptech.glide.Glide

class LovelyAdapter : RecyclerView.Adapter<LovelyAdapter.AnimeViewHolder>() {

    private lateinit var data: List<Data>

    class AnimeViewHolder(private val binding: AnimeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun apply(data: Data) = with(binding) {
            animeListTitle.text = data.attributes.titles.en_jp
            animeListPoster.loadImage(data.attributes.posterImage.original)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AnimeViewHolder {
        val binding = AnimeBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return AnimeViewHolder(binding)
    }

    override fun onBindViewHolder(holder: AnimeViewHolder, position: Int) {
        val item = data[position]
        holder.apply(item)
        holder.itemView.setOnClickListener {
            val action = ListFragmentDirections.actionListFragmentToDetailFragment(item)
            findNavController(it).navigate(action)
        }
    }

    override fun getItemCount(): Int {
        return data.size
    }

    fun applyAnimes(animes: List<Data>) {
        data = animes
    }

}

fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}